#!/usr/bin/python3

from ADsim import kb as ADkb, inm

def main():
	startServices()

def startServices():
	inmFile = "/home/fanda/nets/n3/schema.imn"
	out = "/home/fanda/nets/n3/"
	inNode = "n1"
	# n1
	outNodes = ["n6", "n7", "n8"]
	# n2
	outNodes = ["n28"]
	# n3
	outNodes = ["n9", "n11"]
	# n1
	defs = ["n4", "n7", "n8"]
	# n2
	defs = ["n2", "n5", "n7"]
	# n3
	defs = ["n2", "n3", "n4"] #, "n5", "n6", "n7", "n8", "n10"]
	inm.Inm(inmFile, out, inNode, outNodes, defs)

if __name__ == "__main__":
	main()