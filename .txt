1,"execCode(n12,apache)","OR",0
2,"RULE 7 (remote exploit of a server program)","AND",0
3,"netAccess(n12,tcp,80)","OR",0
4,"RULE 11 (multi-hop access)","AND",0
5,"canConnectToHost(n12)","OR",0
6,"RULE 2 (ATT FOUND ADDRESS)","AND",0
7,"scanAddress(n12)","OR",0
8,"RULE 3 (ATT CAN FIND ADDRESS)","AND",0
9,"execCode(n7,apache)","OR",0
10,"RULE 7 (remote exploit of a server program)","AND",0
11,"netAccess(n7,tcp,80)","OR",0
12,"RULE 11 (multi-hop access)","AND",0
13,"canConnectToHost(n7)","OR",0
14,"RULE 2 (ATT FOUND ADDRESS)","AND",0
15,"scanAddress(n7)","OR",0
16,"RULE 3 (ATT CAN FIND ADDRESS)","AND",0
17,"execCode(n6,apache)","OR",0
18,"RULE 7 (remote exploit of a server program)","AND",0
19,"netAccess(n6,tcp,80)","OR",0
20,"RULE 11 (multi-hop access)","AND",0
21,"canConnectToHost(n6)","OR",0
22,"RULE 1 (ATT KNOW ADDRESS)","AND",0
23,"knowAddress(n6)","LEAF",1
22,23,-1
21,22,-1
20,21,-1
24,"hacl(n6,n6,tcp,80)","LEAF",1
20,24,-1
20,17,-1
19,20,-1
25,"RULE 12 (direct network access)","AND",0
25,21,-1
26,"hacl(internet,n6,tcp,80)","LEAF",1
25,26,-1
27,"attackerLocated(internet)","LEAF",1
25,27,-1
19,25,-1
18,19,-1
28,"networkServiceInfo(n6,httpd,tcp,80,apache)","LEAF",1
18,28,-1
29,"vulExists(n6,'CAN-2002-0392',httpd,remoteExploit,privEscalation)","LEAF",1
18,29,-1
17,18,-1
30,"RULE 8 (remote exploit of a server program)","AND",0
30,19,-1
30,28,-1
30,29,-1
17,30,-1
16,17,-1
31,"canFindAddress(n6,n7)","LEAF",1
16,31,-1
16,21,-1
15,16,-1
14,15,-1
13,14,-1
12,13,-1
32,"hacl(n6,n7,tcp,80)","LEAF",1
12,32,-1
12,17,-1
11,12,-1
33,"RULE 11 (multi-hop access)","AND",0
33,13,-1
34,"hacl(n7,n7,tcp,80)","LEAF",1
33,34,-1
33,9,-1
11,33,-1
10,11,-1
35,"networkServiceInfo(n7,httpd,tcp,80,apache)","LEAF",1
10,35,-1
36,"vulExists(n7,'CAN-2002-0392',httpd,remoteExploit,privEscalation)","LEAF",1
10,36,-1
9,10,-1
37,"RULE 8 (remote exploit of a server program)","AND",0
37,11,-1
37,35,-1
37,36,-1
9,37,-1
8,9,-1
38,"canFindAddress(n7,n12)","LEAF",1
8,38,-1
8,13,-1
7,8,-1
6,7,-1
5,6,-1
4,5,-1
39,"hacl(n12,n12,tcp,80)","LEAF",1
4,39,-1
4,1,-1
3,4,-1
40,"RULE 11 (multi-hop access)","AND",0
40,5,-1
41,"hacl(n7,n12,tcp,80)","LEAF",1
40,41,-1
40,9,-1
3,40,-1
2,3,-1
42,"networkServiceInfo(n12,httpd,tcp,80,apache)","LEAF",1
2,42,-1
43,"vulExists(n12,'CAN-2002-0392',httpd,remoteExploit,privEscalation)","LEAF",1
2,43,-1
1,2,-1
44,"RULE 8 (remote exploit of a server program)","AND",0
44,3,-1
44,42,-1
44,43,-1
1,44,-1
