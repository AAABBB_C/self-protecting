#!/usr/bin/python3

from ADsim import request as ADreq, attackerNetwork as ADnet
import time

class _AutonomeAttack:
	def __init__(self, startingKnowleadge = []):
		self.startingKnowleadge = startingKnowleadge
		self.__setStartingKnowleadge()

	def __setStartingKnowleadge(self):
		for host in self.startingKnowleadge:
			ADnet.Network.addHost(host)
			ADnet.Network.addConnection(ADnet.ATTACKER_NODE, host)

	def start(self, strategy="random"):
		self.strategy = strategy
		self.run = True
		while(self.run):
			nextHost = self.__getNode()
			if not nextHost:
				break

			print("ATTACKING ON " + nextHost)

			out = ADreq.AttackRequest(nextHost, 80).send()
			multipleHops = []
			if out == False:
				scannedHosts = ADnet.Network.getScannedHosts()
				for sHost in scannedHosts:
					paths = ADnet.Network.getAllPaths(sHost, nextHost)
					for p in paths:
						multipleHops = p[1:-1]
						out = ADreq.AttackRequest(nextHost, 80, multipleHops).send()
						if out:
							break
					if out:
						break
						
			print("ATTACK SUCCESS ON " + nextHost)
			
			time.sleep(20)

			knownHosts = ADreq.CmdLocalRequest(nextHost, 80, multipleHops).send('getKnownHosts')
			if knownHosts:
				ADnet.Network.addKnownHosts(nextHost, knownHosts)



	def __getNode(self):
		func = getattr(self, "_" + self.strategy + "Strategy")
		return func()

	def _randomStrategy(self):
		unscanned = ADnet.Network.getUnscannedHosts()
		if len(unscanned) == 0:
			return False
		return unscanned[0]