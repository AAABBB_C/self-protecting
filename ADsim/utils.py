#!/usr/bin/python3

import networkx as nx

def networkxToDotFile(G, file=""):
	try:
		import pygraphviz
		from networkx.drawing.nx_agraph import write_dot
	except ImportError:
		try:
			import pydot
			from networkx.drawing.nx_pydot import write_dot
		except ImportError:
			print()
			print("Both pygraphviz and pydot were not found ")
			print("see  https://networkx.github.io/documentation/latest/reference/drawing.html")
			print()
			raise
	write_dot(G, file)