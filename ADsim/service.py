#!/usr/bin/python3

import socket
import threading
import json
import random
import uuid

from ADsim import request as ADreq, local as ADlocal, monitor as ADmonitor
from ADsim import globalConfig

VULNERABILITIES_SOURCE = "config/vulnerabilities.json"

class RootKey:
	def __init__(self, ovner = "", vulnerability = False, key = ""):
		self.ovner = ovner
		self.vulnerability = vulnerability
		self.key = key

# keys for access to machine, {type_of_key: secret_key_for_access}
ROOT_KEYS = [ RootKey(ovner = "admin", key = "admin") ]

class ThreadedService(object):
	def __init__(self, host, port, serviceName, serviceVulnerabilities):
		self.host = host
		self.port = port
		self.serviceName = serviceName
		self.serviceVulnerabilities = serviceVulnerabilities
		self.customHandler = {}

		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.bind((self.host, self.port))

	def listen(self):
		self.sock.listen(5)
		print("Service '%s' started (port: %s, vuln: %s)" % (self.serviceName, self.port, str(self.serviceVulnerabilities)))
		threading.Thread(target = self._listenLoop).start()

	def _listenLoop(self):
		while True:
			client, address = self.sock.accept()
			print("\n------\nConnection on %s from %s" % (str(self.port), str(address)))
			client.settimeout(60)
			threading.Thread(target = self._listenToClient,args = (client,address)).start()

	def _listenToClient(self, client, address):
		size = 1024
		#try:
		data = client.recv(size)
		if data:
			msgHandle = MessageHandle(data, address, self)
			response = msgHandle.getResponseBinary()
			client.send(response)
		else:
			print('Client disconnected')
		#except Exception as ex:
		#	print("Connection error")
		#	print(ex)
		client.close()
		return False

	def setCustomMessageHandler(self, handler):
		self.customHandler = handler

class MessageHandle:
	def __init__(self, msg, host, service):
		self.msg = msg.decode('ascii')
		self.host = host
		self.service = service
		self.response = {}
		self.__parse()

	def _sendReportMonitor(self, logType, status, data = {}):
		ADmonitor.addLog(logType, self.host, self.service.port, self.service.serviceName, status, data)

	def getResponseObj(self):
		return self.response

	def getResponseBinary(self):
		print("Req:" + self.msg.rstrip())
		print("Resp:" + str(self.response).rstrip())
		if isinstance(self.response, str):
			return (self.response + "\r\n").encode('ascii')

		msg = json.dumps(self.response) + "\r\n"
		
		return msg.encode('ascii')

	def __parse(self):
		if self.service.customHandler:
			self.response = self.service.customHandler.handle(self.msg)
			return

		self.msgObj = json.loads(self.msg)
		print(self.msgObj)
		msgType = next(iter(self.msgObj))
		# calling local method for parse
		func = getattr(self, '_' + self.__class__.__name__ + '__' + msgType)
		func()

	def __attack(self):
		print("ATTACK: " + self.msgObj['attack'])
		attack = AttackHandle(self)
		self.response = attack.getResponse()

	def __request(self):
		print('REQUEST')
		self.response = {'service': self.service.serviceName, 'msg': "running"}

	def __cmd(self):
		cmd = CmdHandle(self)
		self.response = cmd.getResponse()

class CmdHandle:
	def __init__(self, msgHandle):
		self.msgObj = msgHandle.msgObj
		self.cmdReq = self.msgObj['cmd']
		self.validRequest = False
		self.response = {}
		if self.__testAccess():
			print(self.cmdReq)
			if "remote" in self.cmdReq:
				self.remoteCmdReq = self.cmdReq["remote"]
				self.__remoteHandle()
			if "local" in self.cmdReq:
				self.localCmdReq = self.cmdReq["local"]
				self.__localHandle()

	def __testAccess(self):
		validKeys = [x for x in ROOT_KEYS if x.key == self.cmdReq['secret']]
		if len(validKeys) > 0:
			return True
		else:
			return False

	def __remoteHandle(self):
		print("CMD->REMOTE: to %s:%s" % (self.remoteCmdReq["host"], self.remoteCmdReq["port"]))
		remoteMsg = self.remoteCmdReq["msg"]
		self.response = ADreq.SendRequest(self.remoteCmdReq["host"], self.remoteCmdReq["port"])._send(remoteMsg)

	def __localHandle(self):
		self.response = ADlocal.Local().run(self.localCmdReq['shell'])

	def getResponse(self):
		return self.response

class AttackHandle:
	def __init__(self, msgHandle):
		self.msgHandle = msgHandle
		self.msg = msgHandle.msgObj
		self.host = msgHandle.host
		self.vulnName = self.msg['attack']
		self.service = msgHandle.service
		self.attackSucess = False
		self.__loadAttack()

	def __loadAttack(self):
		with open(VULNERABILITIES_SOURCE) as f:
			vulDb = json.load(f)
		if any(x['name'] == self.vulnName for x in vulDb):
			self.config = next((x for x in vulDb if x['name'] == self.vulnName), None)
			self.__handle()
		else:
			print(self.vulnName + " is not in DB")

	def __handle(self):
		if not self.config['service_name'] == self.service.serviceName:
			print('attaker try bad CVS - "' + self.config['service_name'] + '" but it is "' +  self.service.serviceName + '"')
			return
			
		if globalConfig.defense and random.random() < self.config['defense_prob']:
			print("defense success")
			return
		if random.random() < self.config['attack_prob']:
			self.__attackerSuccess()
		if random.random() < self.config['detect_prob']:
			self.__detectSuccess()

	def __attackerSuccess(self):
		#self.msgHandle._sendReportMonitor(ADmonitor.LOG_REMOTE_EXPLOID, ADmonitor.STATUS_SUCC, {'vuln': self.vulnName})
		self.attackSucess = True
		self.rootKey = str(uuid.uuid4())
		print("attacker got access")
		#attacker get root access for this CVS
		attackerKey = RootKey(ovner = self.host, vulnerability=self.vulnName, key = self.rootKey)
		ROOT_KEYS.append(attackerKey)

	def __detectSuccess(self):
		#send message to server
		self.msgHandle._sendReportMonitor(ADmonitor.LOG_REMOTE_EXPLOID, ADmonitor.STATUS_TRY, {'vuln': self.vulnName})
		return ""

	def getResponse(self):
		if self.attackSucess:
			return {'key': self.rootKey}
		else:
			return {'key': False}