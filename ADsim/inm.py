#!/usr/bin/python3

import re
import networkx as nx
import json
import matplotlib
import matplotlib.pyplot as plt
import random

DYNAMIC_PREFIX = "dynamic"
DYNAMIC_CHANGE_PREFIX = "changeDynamic"


class Inm:
	mapping = {}
	def __init__(self, fileName, out, startNode, targetNodes, defs):
		self.fileName = fileName
		self.outFolder = out
		self.startNode = startNode
		self.targetNodes = targetNodes
		self.defs = defs
		self.G = nx.Graph()
		self.createGraph()
		self.generateAttackGraph()
		self.writeDot(self.G, out + "grid.dot")
		self.writeDot(self.AttG, out + "att.dot")
		self.generateMulvalOutput()
		plt.figure()
		nx.draw_networkx(self.G)

	
	def createGraph(self):
		with open(self.fileName, 'r') as myfile:
			data=myfile.read()
		self.fileContent = data
		connections = re.findall(r'link l\d+ \{\s*nodes\s*{(\w\d+)\s+(\w\d+)}', data)
		for conn in connections:
			self.addHost(conn[0])
			self.addHost(conn[1])
			self.addConnection(conn[0], conn[1])
			#print(conn[0] + "-" + conn[1])
			self.getNodeDetail(conn[0])
			self.getNodeDetail(conn[1])

	def getNodeDetail(self, node):
		ipConfig = re.findall(r'(?<=hostname ' + node + ').*?(?=})', self.fileContent, re.S)
		ipAddress = re.findall(r'(?<=ip address ).*?(?=/)', ipConfig[0], re.S)
		#print('node: ' + node)
		for ip in ipAddress:
			if node not in self.mapping:
				self.mapping[node] = []
			if ip not in self.mapping[node]:
				self.mapping[node].append(ip)
			
			#print(ip)
		#print('')

	def addHost(self, host):
		if not host in self.G:
			self.G.add_node(host)

	def addConnection(self, hostA, hostB):
		self.G.add_edge(hostA, hostB)

	def generateAttackGraph(self):
		self.AttG = nx.DiGraph()
		ruleCnt = 0
		idCnt = 0
		for nodeId in self.G.node:
			successors = self.G.neighbors(nodeId)
			for postNode in successors:
				ruleId = 'r' + str(ruleCnt)
				self.AttG.add_edge(nodeId, ruleId)
				self.AttG.add_edge(ruleId, postNode)
				self.AttG.node[ruleId]['label'] = str(idCnt) + ':RULE ' + str(ruleCnt) + '(remote exploit of a server program):0'
				self.AttG.node[ruleId]['shape'] = 'elipse'
				ruleCnt += 1
				idCnt += 1

				self.AttG.node[nodeId]['label'] = str(idCnt) + ':execCode(' + nodeId + ', apache):0'
				self.AttG.node[nodeId]['shape'] = 'diamond'
				idCnt += 1

	def generateMulvalOutput(self):
		keys = list((self.G.node.keys()))
		startNode = self.startNode
		targetNodes = self.targetNodes

		hasAccess = "attackerLocated(internet).\nknowAddress(" + startNode + ").\n\nhacl(H,H,_,_).\nhacl(internet," + startNode + ",_,_).\n"
		canFindHosts = "\n"
		vulnExists = "changeDynamicDefHasAccess('CAN-2002-0392', 'false').\n"
		vulnExists += "vulProperty('CAN-2002-0392', remoteExploit, privEscalation).\n\n\n"
		nettworkSrvInfo = "\n"
		
		attGoals = ""
		attGoalsDef = ""
		for target in targetNodes:
			attGoals += "attackGoal(execCode("+target+",_)).\n"
			#attackGoal(doDefense(server)).
			attGoalsDef += "attackGoal(doDefense("+target+")).\n"

		defHasAccess = ""	
		proactiveDefs = "defAntivirisuCanProtect('CAN-2002-0392').\n"
		for d in self.defs:
			proactiveDefs += "defCanChangeAddress("+d+").\n"
			proactiveDefs += "defCanRunAntiviris("+d+").\n"
			proactiveDefs += "vulExists("+d+", 'CAN-2002-0392', httpd).\n"

		reactiveDefs = ""
		for d in self.defs:
			reactiveDefs += "canChangeAddress("+d+").\n"

		finishEdges = []

		for nodeId in self.G.node:
			nettworkSrvInfo += "networkServiceInfo(" + nodeId + " , httpd, tcp , 80 , apache).\n"
			vulnExists += "vulExists(" + nodeId + ", 'CAN-2002-0392', httpd).\n"
			defHasAccess += "dynamicDefHasAccess(" + nodeId + ", root).\n"
			neighbors = self.G.neighbors(nodeId)
			for postNode in neighbors:
				if (nodeId, postNode) in finishEdges:
					#print("nal")
					a=1
					continue
				finishEdges.append((nodeId, postNode))
				finishEdges.append((postNode, nodeId))
				hasAccess += "hacl(" + nodeId + "," + postNode + ",_,_).\n"
				#hasAccess += "hacl(" + postNode + "," + nodeId + ",_,_).\n"
				canFindHosts += "canFindAddress(" + nodeId + "," + postNode + ").\n"
				#canFindHosts += "canFindAddress(" + postNode + "," + nodeId + ").\n"
		
		out = hasAccess + reactiveDefs + attGoals + canFindHosts + nettworkSrvInfo + vulnExists
		outDirMulval = self.outFolder + 'mulval/'
		import os
		if not os.path.exists(outDirMulval):
			os.makedirs(outDirMulval)

		outFileMul = outDirMulval + "input.P"

		with open(outFileMul, "w") as text_file:
			text_file.write(out)

		os.chdir(self.outFolder + 'mulval')
		os.system("graph_gen.sh input.P -v")
		#/home/fanda/Documents/mul2prism.py

		mulvalAttGraphFile = outDirMulval + "AttackGraph.dot"
		mulvalDefGraphFile = self.outFolder + "mulvalDef/AttackGraph.dot"

		self.G = nx.DiGraph(self.read_dot(mulvalAttGraphFile))
		self.initAttackGraphProbability(outDirMulval)

		self.GDef = nx.DiGraph(self.read_dot(mulvalDefGraphFile))

		self.minimalizeAttackGraph(self.G, outDirMulval)

		outDef = hasAccess + "\n" + canFindHosts + "\n" + attGoalsDef + "\n" + defHasAccess + proactiveDefs
		outDirDefMulval = self.outFolder + 'mulvalDef/'
		if not os.path.exists(outDirDefMulval):
			os.makedirs(outDirDefMulval)

		outFileMul = outDirDefMulval + "input.P"

		with open(outFileMul, "w") as text_file:
			text_file.write(outDef)
		#graph_gen.sh ./input.P -v -r ./../kb/def_interaction_rules.P
		os.chdir(self.outFolder + 'mulvalDef')
		os.system("graph_gen.sh input.P -v -r /home/fanda/Documents/mulval/kb/def_interaction_rules.P")

		os.system("python3 /home/fanda/Documents/mul2prism.py " + outDirMulval + "minimalizedG.dot " + self.outFolder + "input.prism " + outDirDefMulval + "AttackGraph.dot") # > /dev/null

		PRISM_FOLDER = "/home/fanda/Documents/prism-games-2.0.beta3-linux64/bin"
		os.chdir(PRISM_FOLDER)
		#" + self.outFolder + "attackerPrism.props -prop 2 
		#os.system("./prism " + self.outFolder + "input.prism -exportstates sta.sta -exporttrans prismMap.tra" )

	def initAttackGraphProbability(self, path):
		# set boxs and diamonds
		for node in self.G.nodes.data():
			nodeId = node[0]
			if node[1]['shape'] != 'diamond':# and node[1]['shape'] != 'box':
				continue
			nodeProbability = 0.95#random.random()
			nodeLabel = node[1]['label']
			nodeLabelArr = nodeLabel.split(':')
			nodeLabelArr[-1] = str(nodeProbability)
			nodeLabel = ':'.join(nodeLabelArr)
			print(nodeLabel)
			node[1]['label'] = nodeLabel

		# compute ellipse
		#self.G = self.updateAttackGraphRulesProbability(self.G)
		


		self.writeDot(self.G, path + "AttGraphProb.dot")

	def updateAttackGraphRulesProbability(self, G):
		isUpdatedGlobal = False
		while not isUpdatedGlobal:
			for node in G.nodes.data():
				if node[1]['shape'] != 'diamond':
					continue
				nodeId = node[0]
				nodeLabel = node[1]['label']
				nodeLabelArr = nodeLabel.split(':')
				nodeProb = 1.0
				preNodes = G.predecessors(nodeId)
				isUpdated = True
				for preNodeId in preNodes:
					preNode = G.node[preNodeId]
					preNodeLabel = preNode["label"]
					preNodeShape = preNode["shape"]
					preNodeProb = 1.0
					preNodeProbSum = 1.0
					preNodeProbMultiplicaton = 1.0
					if preNodeShape == 'ellipse':
						preRuleNodes = G.predecessors(preNodeId)
						isUpdated = True
						for preRuleNodeId in preRuleNodes:
							preRuleNodeShape = G.node[preRuleNodeId]['shape']
							preRuleNodeLabel = G.node[preRuleNodeId]['label']
							if not preRuleNodeShape == 'diamond':
								continue
							preRuleNodeLabelArr = preRuleNodeLabel.split(':')
							preRuleNodeProb = float(preRuleNodeLabelArr[-1])
							if preRuleNodeProb < 0.0:
								isUpdated = False
								break
						# some of precondition nodes are not inicialized
						if not isUpdated:
							break
						
						firstPreRuleNode = True
						for preRuleNodeId in preRuleNodes:
							preRuleNodeLabel = G.node[preRuleNodeId]['label']
							#print(preRuleNodeLabel)
							preRuleNodeLabelArr = preRuleNodeLabel.split(':')
							preRuleNodeProb = float(preRuleNodeLabelArr[-1])
							if firstPreRuleNode:
								firstPreRuleNode = False
								preNodeProbSum = preNodeProbSum + preRuleNodeProb
							else:
								preNodeProbSum = preNodeProbSum + preRuleNodeProb - (preNodeProbSum * preRuleNodeProb)
							#preNodeProbMultiplicaton = preNodeProbMultiplicaton * preRuleNodeProb
							#print(str(preRuleNodeProb) + " SUM:" + str(preNodeProbSum) + " - MULT:" + str(preNodeProbMultiplicaton))
						preNodeProb = preNodeProbSum# - preNodeProbMultiplicaton
					else:
						preNodeLabelArr = preNodeLabel.split(':')
						preNodeProb = float(preNodeLabelArr[-1])
					nodeProb = nodeProb * preNodeProb
				
				if not isUpdated:
					isUpdatedGlobal = False
					continue
				else:
					isUpdatedGlobal = True
				
				nodeProb = abs(float(nodeLabelArr[-1]) * nodeProb)
				nodeLabelArr[-1] = str(nodeProb)
				nodeLabel = ':'.join(nodeLabelArr)
				G.node[nodeId]['label'] = nodeLabel
				print("update " + nodeLabel)

		return G

	def minimalizeAttackGraph(self, file, path):
		observedNodes = ['41', '33', '25', '17', '90', '9', '1']
		self.minimazedG = file
		self.edgesToAdd = []
		self.nodesToDel = []
		self.nodeFinishd = []
		
		

		while self.minimalizeRecursion(observedNodes):
			x = 1
			#self.updateAttackGraphRulesProbability(self.minimazedG)

		self.minimalizeNew(observedNodes)

		
		self.writeDot(self.minimazedG, path + "minimalizedG.dot")

	def replaceSpecChars(self, inStr):
		inStr = re.sub(r'\d+:', '', inStr, 1)
		inStr = re.sub(r'\):\d+', '', inStr, 1)
		inStr = inStr.replace('(', '_')
		inStr = inStr.replace(')', '_')
		inStr = inStr.replace(':', '_')
		inStr = inStr.replace(',', '_')
		inStr = inStr.replace(' ', '_')
		inStr = inStr.replace('-', '_')
		inStr = inStr.replace('.', '_')
		inStr = inStr.replace('\'', '_')
		inStr = inStr.replace('\"', '_')
		return inStr

	def actionName(self, node, preCondId):
		name = node[1]['label']
		name = self.replaceSpecChars(name)
		out = 'n' + preCondId + '__' + name #[2:]
		#print(out)
		return out

	def getProbabilityFromLabel(self, label):
		labelArr = label.split(':')
		prob = float(labelArr[-1])
		return prob
	
	def getNameFromLabel(self, label):
		labelArr = label.split(':')
		name = labelArr[1]
		return name
	
	def getPrismNameFromLabel(self, label, mashineName):
		labelArr = label.split(':')
		if len(labelArr) > 1:
			name = labelArr[1]
		else:
			name = label
		name = self.replaceSpecChars(name)
		
		if name.startswith(DYNAMIC_PREFIX):
			name = name[len(DYNAMIC_PREFIX):]
			name = name[0].lower() + name[1:]
		elif name.startswith(DYNAMIC_CHANGE_PREFIX):
			name = name[len(DYNAMIC_CHANGE_PREFIX):]
			name = name[0].lower() + name[1:]
		name = name.split('_')
		return name[0] + '_' + mashineName

	def addToVar(self, name, value, vars):
		if not any(x['name'] == name for x in vars):
			varib = {}
			varib['name'] = name
			varib['value'] = value
			vars.append(varib)
		return vars

	def minimalizeNew(self, observedNodes):
		allPaths = []

		quene = ['41']
		index = 0
		while index < len(quene):
			nodeId = quene[index]
			if self.minimazedG.node[nodeId]['shape'] == 'diamond':
				postRules = self.minimazedG.successors(nodeId)
				for postRuleId in postRules:
					postNodes = self.minimazedG.successors(postRuleId)
					for postNodeId in postNodes:
						if self.minimazedG.node[postNodeId]['shape'] == 'diamond':
							if not postNodeId in quene:
								quene.append(postNodeId)
							paths = list(nx.all_shortest_paths(self.G, source=nodeId, target=postNodeId))
							for path in paths:
								if path not in allPaths:
									allPaths.append(path)
			index = index + 1

		prismAttackerRules = []
		attackerStates = []
		prismGlobalVars = []

		for path in allPaths:
			node = self.G.node[nodeId]
			name = self.getNameFromLabel(node['label'])
			
			nodeObj = MulvalNode(node['label'])
			mashineName = nodeObj.params[0]
			stateName = "node" + nodeId + "_" + self.replaceSpecChars(name)
			pathPrismGuard = "[" + stateName + "] node"+path[0] + " & "
			if not stateName in attackerStates:
				attackerStates.append(stateName)

			pathPrismProbability = 1.0
			prismUpdate = ""
			for nodeId in path:
				node = self.G.node[nodeId]
				if node['shape'] == 'ellipse':
					ruleNodes = self.G.predecessors(nodeId)
					#print(ruleNodes)
					for ruleNodeId in ruleNodes:
						ruleNode = self.G.node[ruleNodeId]
						ruleNodeLabelName = self.getNameFromLabel(ruleNode['label'])
						#print("AAAAAAA")
						if ruleNode['shape'] == "box" and ruleNodeLabelName.startswith(DYNAMIC_PREFIX):
							#print("AAAAAAA")
							normPrismName = self.getPrismNameFromLabel(ruleNode['label'], mashineName)
							#normPrismName = self.replaceSpecChars(name)
							#print(normPrismName)
							#name = name[len(DYNAMIC_PREFIX):]
							# first letter lower case
							#normPrismName = normPrismName[0].lower() + normPrismName[1:] + mashineName
							pathPrismGuard += normPrismName + " & "
							prismGlobalVars = self.addToVar(normPrismName, 'true', prismGlobalVars)

						elif ruleNode['shape'] == "box" and ruleNodeLabelName.startswith(DYNAMIC_CHANGE_PREFIX):
							normPrismName = self.getPrismNameFromLabel(ruleNode['label'], mashineName)
							#name = self.replaceSpecChars(name)
							#name = name[len(DYNAMIC_CHANGE_PREFIX):]
							# first letter lower case
							#normPrismName = name[0].lower() + name[1:] + mashineName
							#parms = normPrismName.split('_')
							#normPrismName = parms[0]
							prismGlobalVars = self.addToVar(normPrismName, 'false', prismGlobalVars)
							prismUpdate += '(' + normPrismName + '\'=' + 'true' + ')'
				if node['shape'] == 'diamond':
					nodeProbability = self.getProbabilityFromLabel(node['label'])
					rulesCount = len(list(self.G.predecessors(nodeId)))
					pathPrismProbability = pathPrismProbability * (nodeProbability/rulesCount)
			
			prismGlobalVars = self.addToVar('node'+nodeId, 'false', prismGlobalVars)
			prismUpdate += '(' + 'node'+nodeId + '\'=true)'

			prismRule = pathPrismGuard[:-3] + "-> " + str(pathPrismProbability) + ': ' + prismUpdate
			prismRule += " + " + str(1-pathPrismProbability) + ": true;"
			prismAttackerRules.append(prismRule)



		# GENERATE DEFENSE
		defenderStates = []
		prismDefenderRules = []
		prismPreconditions = {}
		GDef = self.GDef

		for node in GDef.nodes.data():
			nodeId = node[0]
			#current node name
			#nodeVarName = 'nodeDef' + nodeId
			#we must create global variable of current node
			#prismGlobalVar += 'global ' + nodeVarName + ' : bool init false;\n'
			preConds = GDef.predecessors(nodeId)
			nodeObj = MulvalNode(GDef.node[nodeId]['label'])
			

			for preCondId in preConds:
				preCondShape = GDef.node[preCondId]['shape']
				if preCondShape != "ellipse":
					continue
				#print(preCondId)
				#get all diamonds witch are connected to ellipse
				prismNodeLabel = GDef.node[preCondId]['label']
				
				defName = re.findall(r'\(([\w\W]+)\)', prismNodeLabel)[0]
				defName = self.replaceSpecChars(defName)
				if defName == "NO_DEFENSE":
					continue
				defName = self.getPrismNameFromLabel(prismNodeLabel, nodeObj.params[0])
				#defName += "_" + nodeObj.params[0]
				preCondNodes = GDef.predecessors(preCondId)
				prismNodeAction = self.actionName(node, preCondId)
				prismNodePreconds = ''#'defRes>0&'
				prismNodePrecondsUpdate = '(' + defName + '\'=true)'
				isDefenderPreconds = False
				prismGlobalVars = self.addToVar(defName, 'false', prismGlobalVars)
				
				#add each diamond to pre-condition
				for preCondNodeId in preCondNodes:
					preCondNodeName = 'nodeDef' + preCondNodeId
					preCondNodeLabel = GDef.node[preCondNodeId]['label']
					preCondNodeShape = GDef.node[preCondNodeId]['shape']
					if preCondNodeShape == "box":
						preCondNodeObj = MulvalNode(preCondNodeLabel)
						#preCondNodePrism = preCondNodeObj.actionName + "_" + preCondNodeObj.params[0]
						preCondNodePrism = self.getPrismNameFromLabel(preCondNodeObj.actionName, preCondNodeObj.params[0])
						if not preCondNodeObj.actionName.startswith(DYNAMIC_PREFIX):
							continue
						prismNodePreconds += preCondNodePrism + '&'
						prismPreconditions[preCondNodePrism] = "true"

				#node do not have precondition
				if isDefenderPreconds and len(prismNodePreconds) == 0:
					continue

				#remove last & character
				prismNodePreconds = prismNodePreconds[:-1]

				if not stateName in defenderStates:
					defenderStates.append(prismNodeAction)


				prismNodeAction = '	[' + prismNodeAction + ']'

				probSucc = 0.95
				probFail = 1 - probSucc
				probSucc = str(probSucc)
				probFail = str(probFail)
				prismDefenderRule = prismNodeAction + ' ' + prismNodePreconds + ' -> ' + probSucc + ': ' + prismNodePrecondsUpdate +  ' + ' + probFail + ': true;' #
				prismDefenderRules.append(prismDefenderRule)




		prismOutput = 'smg\n\n'
		prismOutput += 'player a1\n'
		prismOutput += '	attacker1, '
		for aState in attackerStates:
			prismOutput += '[' + aState + '], '
		prismOutput = prismOutput[:-2]
		prismOutput += '\nendplayer\n\n\n\n'
		
		prismOutput += 'player def\n'
		prismOutput += '	defender, '
		for aState in defenderStates:
			prismOutput += '[' + aState + '], '
		prismOutput = prismOutput[:-2]
		prismOutput += '\nendplayer\n\n\n\n'
		


		#global node1 : bool init 
		for globalVar in prismGlobalVars:
			prismOutput += "global " + globalVar['name'] + ' : bool init ' + globalVar['value'] + ";\n"
		prismOutput += '\n\n\n'

		prismOutput += 'module attacker1\n'
		prismOutput += '\n	'.join(prismAttackerRules)
		prismOutput += '\nendmodule\n'
		
		prismOutput += 'module defender\n'
		prismOutput += '\n	'.join(prismDefenderRules)
		prismOutput += '\nendmodule\n'

		prismOutput += 'module sched\n'
		prismOutput += '	t: [1..2] init 1;\n\n'
		for aState in attackerStates:
			prismOutput += "	[" + aState + "] t=1 -> (t'=2);"+'\n'
		for aState in defenderStates:
			prismOutput += "	[" + aState + "] t=2 -> (t'=1);"+'\n'
		prismOutput += '\nendmodule\n'

		with open(self.outFolder + "prism_new.prism", "w") as text_file:
			print(f"{prismOutput}", file=text_file)


# OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD
	def minimalizeRecursion(self, observedNodes ):

		for node in self.minimazedG.nodes.data():
			nodeId = node[0]
			if (not 'shape' in node[1] or node[1]['shape'] != 'diamond') or nodeId in self.nodeFinishd:
				continue
			#print(node[1])
			if not nodeId in observedNodes:
				ruleNodes = self.minimazedG.predecessors(nodeId)
				postNodes = list(self.minimazedG.successors(nodeId))
				#print("pocet post>>" + str(len(postNodes)))
				for ruleNodeId in ruleNodes:
					if not ruleNodeId in self.minimazedG.node:
						continue
					ruleNodeData = self.minimazedG.node[ruleNodeId]
					#print("RULE   - " + str(ruleNodeData))
					preRuleNodes = self.minimazedG.predecessors(ruleNodeId)
					for preRuleNodeId in preRuleNodes:
						#print("PRERULE   -" + str(self.minimazedG.node[preRuleNodeId]))
						
						for postNodeId in postNodes:
							#print("POSTRULE   -" + str(self.minimazedG.node[postNodeId]))
							edgeName = preRuleNodeId + "---" + postNodeId
							#if not edgeName in self.edgesToAdd:
							#	self.edgesToAdd.append(edgeName)
							if not self.minimazedG.has_edge(preRuleNodeId, postNodeId):
								self.minimazedG.add_edge(preRuleNodeId, postNodeId)
							#return True
					self.nodesToDel.append(ruleNodeId)
					self.minimazedG.remove_node(ruleNodeId)
					return True
				self.nodesToDel.append(nodeId)

				self.minimazedG.remove_node(nodeId)
				
				self.nodeFinishd.append(nodeId)
				return True
			else:
				self.nodeFinishd.append(nodeId)
			#print()
			#print()
			#print()

		# CLEANING
		for node in list(self.minimazedG.nodes.data()):
			nodeId = node[0]
			ruleNodes = list(self.minimazedG.predecessors(nodeId))
			postNodes = list(self.minimazedG.successors(nodeId))
			if len(ruleNodes) == 0 and len(postNodes) == 0:
				self.minimazedG.remove_node(nodeId)
				a=1

		return False


	def read_dot(self, path):
		"""Return a NetworkX graph from a dot file on path.

		Parameters
		----------
		path : file or string
		File name or file handle to read.
		"""
		try:
			import pygraphviz
		except ImportError:
			raise ImportError('read_dot() requires pygraphviz ',
							'http://pygraphviz.github.io/')
		A=pygraphviz.AGraph(file=path)
		return A

	def writeDot(self, G, file=""):
		try:
			import pygraphviz
			from networkx.drawing.nx_agraph import write_dot
			print("using package pygraphviz")
		except ImportError:
			try:
				import pydot
				from networkx.drawing.nx_pydot import write_dot
				print("using package pydot")
			except ImportError:
				print()
				print("Both pygraphviz and pydot were not found ")
				print("see  https://networkx.github.io/documentation/latest/reference/drawing.html")
				print()
				raise

		#G = nx.grid_2d_graph(5, 5)  # 5x5 grid
		write_dot(G, file)
		#print("Now run: neato -Tps grid.dot >grid.ps")

		s = json.dumps(self.mapping)
		with open(self.outFolder + "mapping.json", "w") as text_file:
			print(f"{s}", file=text_file)

class MulvalNode:
	id = ""
	actionName = ""
	params = []
	attacks = []
	deffenses = []

	def __init__(self, label):
		self.parse(label)

	def parse(self, label):
		nodeIdMatchObj = re.match(r'\d+:', label)
		self.id = nodeIdMatchObj.group(0)[0:-1]
		
		cleanMsg = re.sub(r'\d+:', '', label, 1)
		cleanMsg = re.sub(r':\d+.*', '', cleanMsg)

		allParams = re.findall(r'\w+', cleanMsg)
		self.actionName = allParams[0]
		if len(allParams) > 1:
			self.params = allParams[1:]

	def __str__(self):
		return str(self.__class__) + ": " + str(self.__dict__)


