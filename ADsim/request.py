#!/usr/bin/python3
import socket
import json
import jsonpickle

from ADsim import attackerNetwork as ADnetwork

VULNERABILITIES_SOURCE = "config/vulnerabilities.json"

NICE_OUT = True

#
#	STRUCTURES
#

class MsgStruct(object):
	def __init__(self, attack = "", request = "", cmd = False):
		if len(attack) > 0:
			self.attack = attack
		elif len(request) > 0:
			self.request = request
		elif cmd:
			self.cmd = cmd

class CmdMsgStruct(object):
	def __init__(self, secret, remote = {}, local = {}):
		self.secret = secret
		if remote:
			self.remote = remote
		if local:
			self.local = local

class RemoteCmdMsgStruct(object):
	def __init__(self, host, port, msg):
		self.host = host
		self.port = port
		self.msg = msg

class LocalCmdMsgStruct(object):
	def __init__(self, shell):
		self.shell = shell


#
#	CLASSES
#


class SendRequest(object):
	def __init__(self, host, port, multiHop=[], data={}):
		self.host = host
		self.port = port
		self.multiHop = multiHop
		self.data = data

	def __connect(self):
		try:
			self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.sock.connect((self.host, self.port))
		except:
			print('c err')

	def send(self, request, response = True, parseJson = True):
		self._send(request, response = True, parseJson = True)

	def _send(self, request, response = True, parseJson = True):
		if len(self.multiHop) > 0:
			try:
				request = self.__buildMultiHopRequest(request)
			except ValueError as err:
				if not NICE_OUT:
					print("REQ FAIL (%s): %s" % (self.host + ':' + str(self.port), str("Is not hacked")))
				return False

		else:
			self.__connect()
		
		msg = jsonpickle.encode(request, unpicklable=False) + "\r\n"
		if not NICE_OUT:
			print("REQ (%s): %s" % (self.host + ':' + str(self.port), str(msg)))
		try:
			self.sock.send(msg.encode('ascii'))
		except:
			if not NICE_OUT:
				print("REQ FAIL (%s): %s" % (self.host + ':' + str(self.port), str(msg)))
			return False
		out = True
		if response:
			if parseJson:
				resp = self._getResponse()
				if not NICE_OUT:
					print("RESP: " + str(resp))
				try:
					out = json.loads(resp)
				except:
					out = resp
			else:
				out = self._getResponse()

		self.sock.close()
		return out

	def __buildMultiHopRequest(self, origRequest):
		#backup target
		targetHost = self.host
		targerPort = self.port
		
		#create remote to target
		remote = RemoteCmdMsgStruct(host = self.host, port = self.port, msg = origRequest)

		tmpMultihop = self.multiHop.copy()
		#get first host, because first request will be to this host
		firstHost = tmpMultihop[0]
		del tmpMultihop[0]
		quene = reversed(tmpMultihop)

		#starting with last host before target
		for host in quene:
			hostKey = self._getHostKey(host)
			if hostKey == False:
				raise ValueError('Host does not have a key (is not hacked yet)')
			cmd = CmdMsgStruct(secret=hostKey.secret, remote=remote)
			msg = MsgStruct(cmd=cmd)
			remote = RemoteCmdMsgStruct(host=hostKey.host, port=hostKey.port, msg=msg)
			
		#get host key to first host
		firstHostKey = self._getHostKey(firstHost)
		if firstHostKey == False:
				raise ValueError('Host does not have a key (is not hacked yet)')
		#construct msg for first host
		cmd = CmdMsgStruct(secret=firstHostKey.secret, remote=remote)
		msg = MsgStruct(cmd=cmd)

		#set connection to first host
		self.host = firstHostKey.host
		self.port = firstHostKey.port

		#connect to first host
		self.__connect()

		#restore backupt to target
		self.host = targetHost
		self.port = targerPort

		return msg

	def _getHostKey(self, host):
		keys = ADnetwork.Network.getHostKeys(host)
		# TODO check if key is still valid + auto attack if not
		if len(keys) > 0:
			return keys[0]
		else:
			return False

	def _getResponse(self):
		msg = self.sock.recv(1024)
		return msg.decode('ascii')

	def _autoAddNetworkConnections(self):
		if len(self.multiHop) > 0:
			tmpArr = self.multiHop.copy()
			tmpArr.insert(0, ADnetwork.ATTACKER_NODE)
			tmpArr.append(self.host)
			for i in range(len(tmpArr) - 2):
				ADnetwork.Network.addConnection(tmpArr[i], tmpArr[i+1])
		else:
			ADnetwork.Network.addConnection(ADnetwork.ATTACKER_NODE, self.host)


class AttackRequest(SendRequest):
	def send(self, repeat = 10):
		self.repeat = repeat
		return self.__attack()

	def __attack(self):
		if not hasattr(self.data, 'serviceName'):
			self.data['serviceName'] = self.__getServiceName()
			if self.data['serviceName'] == False:
				return False
		if not hasattr(self.data, 'vulnerability'):
			return self.__attackAuto()
		else:
			return 

	def __getServiceName(self):
		req = MsgStruct(request='test')
		resp = self._send(req)
		if resp == False:
			return False
		if not NICE_OUT:
			print("Got service name: " + str(resp['service']))
		return resp['service']

	def __attackAuto(self):
		if not NICE_OUT:
			print("Auto attack")
		with open(VULNERABILITIES_SOURCE) as f:
			vulDb = json.load(f)
		#if any(x['name'] == self.data.serviceName for x in vulDb):
		for vulnConfig in [x for x in vulDb if x['service_name'] == self.data['serviceName']]:
			resp = self.__attackRemoteExploit(vulnConfig['name'])
			if resp:
				return resp

	def __attackRemoteExploit(self, expName):
		if not NICE_OUT:
			print("Attacking " + expName)
		req = MsgStruct(attack=expName)
		# repat moretime
		for i in range(self.repeat):
			resp = self._send(req)
			# if success, save access key
			if resp['key']:
				key = ADnetwork.KeyStruct(host = self.host, port = self.port, vulnerability = expName, secret = resp['key'])
				hostStruct = ADnetwork.HostDataStruct(scanned=True, keys=[])
				ADnetwork.Network.addHost(self.host, hostStruct)
				ADnetwork.Network.addHostKey(self.host, key)
				ADnetwork.Network.setHostScanned(self.host)
				print('GOT KEY: ' + self.host + ':' + str(self.port) + ' - ')
				return resp
		return False

class CmdRemoteRequest(SendRequest):
	def send(self, msg, key = False):
		self.msg = msg
		self.key = key
		if not key:
			self.key = self._getHostKey(self.host)
			if not NICE_OUT:
				print('AUTO SELECT KEY: ' + str(self.key))
		self.secret = self.key.secret
		return self.__cmd()

	def __cmd(self):
		remoteMsg = RemoteCmdMsgStruct(host=self.host, port=self.port, msg=self.msg)
		cmdMsg = CmdMsgStruct(secret=self.secret, remote=remoteMsg)
		msg = MsgStruct(cmd=cmdMsg)
		return self._send(msg)

class CmdLocalRequest(SendRequest):
	def send(self, shell, key = False):
		self.shell = shell
		self.key = key
		if not key:
			self.key = self._getHostKey(self.host)
			if not NICE_OUT:
				print('AUTO SELECT KEY: ' + str(self.key))
			if self.key == False:
				return False
		self.secret = self.key.secret
		return self.__cmd()

	def __cmd(self):
		local = LocalCmdMsgStruct(shell=self.shell)
		cmd = CmdMsgStruct(secret=self.secret, local=local)
		msg = MsgStruct(cmd=cmd)
		return self._send(msg)