#!/usr/bin/python3

import threading
import queue
import time

from ADsim import request as ADreq

import socket

SERVER_ADDRESS = '127.0.0.1'
SERVER_PORT = 9999
SERVER_REFRESH_TIME = 1

LOG_REMOTE_EXPLOID = 1

STATUS_TRY = 1
STATUS_SUCC = 2
STATUS_ERR = 3

_guene = queue.Queue()
_queueLock = threading.Lock()

def addLog(logType, srcIp, port, serviceName, status, data = {}):
	global _guene
	global _queueLock

	out = {'type': logType, 'srcIp': srcIp, 'port': port, 'status': status, 'serviceName': serviceName, 'host': socket.gethostname(), 'data': data}

	_queueLock.acquire()
	_guene.put(out)
	_queueLock.release()


class _Monitor(threading.Thread):
	def __init__(self, guene, lock):
		threading.Thread.__init__(self)
		self.guene = guene
		self.lock = lock

	def run(self):
		while(True):
			self.__processData()

	def __processData(self):
		self.lock.acquire()
		if not self.guene.empty():
			data = self.guene.get()
			self.lock.release()
			self.__reportData(data)
		else:
			self.lock.release()
			time.sleep(SERVER_REFRESH_TIME)

	def __reportData(self, data):
		ADreq.SendRequest(SERVER_ADDRESS, SERVER_PORT).send(data, False)





Monitor = _Monitor(_guene, _queueLock)

def runMonitor():
	Monitor.start()