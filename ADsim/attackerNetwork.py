#!/usr/bin/python3

import networkx as nx
from typing import NamedTuple
import matplotlib

import matplotlib.pyplot as plt



ATTACKER_NODE = "attacker"

class KeyStruct(NamedTuple):
	host: str
	port: int
	vulnerability: str
	secret: str

class HostDataStruct(NamedTuple):
	keys: list = []
	scanned: bool = False


class _Network:
	def __init__(self):
		self.G = nx.DiGraph()
		self.addHost(ATTACKER_NODE, hostData=HostDataStruct(scanned=True))

	def addHost(self, host, hostData = {}):
		if not host in self.G:
			if not hostData:
				hostData = HostDataStruct([], False)
			self.G.add_node(host, data=hostData)

	def addHostKey(self, host, key):
		if host in self.G:
			self.G.node[host]['data'].keys.append(key)

	def getHostKeys(self, host):
		if host in self.G:
			return self.G.node[host]['data'].keys
		else:
			return []

	def addConnection(self, hostA, hostB):
		self.G.add_edge(hostA, hostB)

	def addKnownHosts(self, srcHost, knownHosts):
		for newHost in knownHosts:
			self.addHost(newHost['host'])
			self.addConnection(srcHost, newHost['host'])

	def getUnscannedHosts(self):
		filtered = [ x for x, y in self.G.nodes(data=True)  if y['data'].scanned == False ]
		return filtered

	def getScannedHosts(self):
		filtered = [ x for x, y in self.G.nodes(data=True)  if y['data'].scanned == True ]
		return filtered

	def setHostScanned(self, host):
		rpl = self.G.node[host]['data']._replace(scanned = True)
		self.G.node[host]['data'] = rpl
		#print(self.G.nodes(data=True))

	def getAllPaths(self, src, dest):
		return list(nx.all_simple_paths(self.G, source=src, target=dest))

	def draw(self):
		plt.plot([1, 2, 3])
		plt.show()
		nx.draw(self.G, with_labels=True, font_weight='bold')
		plt.show()


# global
Network = _Network()