#!/usr/bin/python3

import json

clientConfigFile = "config/host1.json"
config = {}

defense = False

def getClientConfig(configFile = ""):
	#global config
	global clientConfigFile
	#if config:
	#	return config

	if len(configFile) > 1:
		clientConfigFile = configFile
	with open(clientConfigFile) as f:
		config = json.load(f)
	return config

mappingFile = "/home/fanda/mapping.json"
def getIpFromHostname(host):
	with open(mappingFile) as f:
		mapping = json.load(f)
	
	return mapping[host]