#!/usr/bin/python3

import jsonpickle

import ADsim.globalConfig as config

class Local:
	def __init__(self):
		self.shell = ""

	def run(self, shell):
		self.shell = shell
		self.__callCommand()
		return self.output

	def __callCommand(self):
		func = getattr(self, self.shell)
		func()

	def getKnownHosts(self):
		clientCnf = config.getClientConfig()
		self.output = clientCnf['known_hosts']
		return self.output

	def runDefense(self):
		print("DEFENSE RUNNED")
		config.defense = True
		self.output = "DEF RUNNDED - OK"