#!/usr/bin/python3

import socket
import threading
import json
import re
import networkx as nx
import time

from ADsim import service as ADservice
from ADsim import utils
from ADsim import globalConfig as config
from ADsim import request as ADreq
from ADsim import attackerNetwork as ADattNetwork


SERVER_PORT = 9999
SERVER_SERVICE_NAME = "DEFENDER"

NICE_OUT = False

defResources = 3

SOURCE_DIRECTORY = "/home/fanda/nets/n3/"

PRISM_GLOBAL = {}

class MulvalNode:
	id = ""
	actionName = ""
	params = []
	attacks = []
	deffenses = []

	def __init__(self, label):
		self.parse(label)

	def parse(self, label):
		nodeIdMatchObj = re.match(r'\d+:', label)
		self.id = nodeIdMatchObj.group(0)[0:-1]
		
		cleanMsg = re.sub(r'\d+:', '', label, 1)
		cleanMsg = re.sub(r':\d+.*', '', cleanMsg)

		allParams = re.findall(r'\w+', cleanMsg)
		self.actionName = allParams[0]
		if len(allParams) > 1:
			self.params = allParams[1:]

	def __str__(self):
		return str(self.__class__) + ": " + str(self.__dict__)




class Attack:
	def __init__(self, attacker = "", host = "", timestamp = 0, vulnerability = "", attType = 0, state = 0):
		self.attacker = attacker
		self.host = host
		self.timestamp = timestamp
		self.vulnerability = vulnerability
		self.attType = attacker
		self.state = state


def read_dot(path):
	try:
		import pygraphviz
	except ImportError:
		raise ImportError('read_dot() requires pygraphviz ',
						  'http://pygraphviz.github.io/')
	A=pygraphviz.AGraph(file=path)
	return A




class _KB:
	def __init__(self, mulvalSrc):
		self.mulvalSrc = mulvalSrc
		self.__monitoringCriticalNodes = []
		self.__lastAttackPosition = []
		self.__currAttackPosition = []
		self.__loadMulvalFile()
		self.__lastAttackPosition = ['41']
		self.parsePrismFile()
		#self.cutGraph()
		#Strategy.parsePrismStates(self)
		#print(self.G.nodes(data=True))

	def run(self):
		print("KB is starting...")
		self.service = ADservice.ThreadedService('', SERVER_PORT, SERVER_SERVICE_NAME, [])
		self.service.setCustomMessageHandler(self)
		self.service.listen()

	def handle(self, msg):
		self.msg = msg
		msgObj = json.loads(self.msg)
		self.__findMulvalNode(msgObj)
		#print(self.G.nodes(data=True))
		strat = Strategy(self, self.G, self.__lastAttackPosition, self.__currAttackPosition, ["1"]) #self.__monitoringCriticalNodes)
		

	def __findMulvalNode(self, msg):
		actionName = "execCode"
		print('HOST ' + msg['host'] + " " + actionName)
		#print()
		#print("scanning..............................")
		self.__currAttackPosition = []
		for node in self.G.nodes.data():
			nodeId = node[0]
			nodeLabel = self.G.node[nodeId]['label']
			if node[1]['shape'] == 'ellipse':
				continue
			if re.match(r'\d+:def[A-Z]{1}.*', nodeLabel, re.M):
				continue

			nodeObj = MulvalNode(nodeLabel)

			#print(nodeObj.params[0] + ":" + self.G.node[nodeId]['data'].actionName)
			if self.G.node[nodeId]['data'].actionName == actionName and nodeObj.params[0] == msg['host']:
				#print("math" + nodeId)
				self.__currAttackPosition.append(nodeId)
				att = Attack(host=msg['srcIp'], timestamp=time.time(), state=msg['status'])
				node[1]['shape'] = 'doublecircle'
				self.G.node[nodeId]['data'].attacks = att

		#print(self.G.nodes.data())
		#print("scanning..............................")
		#utils.networkxToDotFile(self.G, "/home/fanda/tet.dot")
		return

	def __loadMulvalFile(self):
		self.G = nx.DiGraph( read_dot(self.mulvalSrc) )
		filterBoxs = [x for x,y in self.G.nodes(data=True) if y['shape']=='box']
		self.attackerNodes = []
		for node in filterBoxs:
			self.G.remove_node(node)
		
		filterBoxsDef = []

		for node in self.G.nodes.data():
			#we want only diamond
			if node[1]['shape'] == 'ellipse':
				continue

			nodeId = node[0]
			#print(nodeId)

			if len(self.__monitoringCriticalNodes) == 0:
				self.__monitoringCriticalNodes = [nodeId]

			self.__lastAttackPosition = [nodeId]

			nodeLabel = self.G.node[nodeId]['label']
			#we want only attacker step
			if re.match(r'\d+:def[A-Z]{1}.*', nodeLabel, re.M):
				#filterBoxsDef.append(nodeId)
				continue

			#get defense
			nodeDefense = []
			preConds = self.G.predecessors(nodeId)
			for preCondId in preConds:
				preCondNodes = self.G.predecessors(preCondId)
				for preCondNodeId in preCondNodes:
					preCondNodeLabel = self.G.node[preCondNodeId]['label']
					if re.match(r'\d+:def[A-Z]{1}.*', preCondNodeLabel, re.M):
						nodeDefense.append(preCondNodeId)

			self.attackerNodes.append(nodeId)

			#print(nodeLabel)
			mulNode = MulvalNode(nodeLabel)
			mulNode.deffenses = nodeDefense
			#print(len(mulNode.deffenses))
			#print()
			#print()
			#print(str(mulNode))
			self.G.node[nodeId]['data'] = mulNode

		for node in filterBoxsDef:
			self.G.remove_node(node)

	def parsePrismFile(self):
		global PRISM_GLOBAL

		file = SOURCE_DIRECTORY + "input.prism"
		with open(file, 'r') as myfile:
			data=myfile.readlines()
		
		for line in data:
			if re.match("global (DEF|dynamic)", line):
				words = line.split(" ")
				last = words[-1]
				value = last[:-1]
				name = words[1]
				PRISM_GLOBAL[name] = value




class Strategy:

	def __init__(self, KB, G, lastAttackPosition, currAttackPosition, monitoringCriticalNodes):
		self.G = G
		self.__lastAttackPosition = lastAttackPosition
		self.__currAttackPosition = currAttackPosition
		self.__monitoringCriticalNodes = monitoringCriticalNodes
		#self.__monitoringCriticalNodes = currAttackPosition
		self.__attackerNodes = KB.attackerNodes

		#print("Last pos")
		#print(lastAttackPosition)

		if not NICE_OUT:
			print("Last pos")
			print(lastAttackPosition)
		print("new pos")
		print(currAttackPosition)

		# nacteni souboru
		# idStavu - kam se ma v nem pokracovat
		self.parsePrismStrategy()

		# nacteni souboru
		# ulozeni nazvu k idStavu
		self.parsePrismTransitions()
		# nacteni souboru
		# kombinace node1, node2 ... na idStavu
		self.parsePrismStates()
		# vstup - pole starych pozic utocnika, pole akt pozic
		# vystup - vsechny mozne cesty mezi temito body
		self.__findAllPaths()

		#print('poss paths')
		#print((self.__attackPaths))
		#self.__getAllEnds()
		#print(self.__lastAttackPosition)
		#print("last pos")
		#print(self.__lastAttackPosition)
		
		# vsechny cesty mezi posledni pozici a kritickymi uzly
		#self.__getAllPossibleNextPaths()

		#najde vsechny cesty kde je mozna obrana
		#self.__getPathWithDefense()
		self.__doDefense()

	def __findAllPaths(self):
		self.__attackPaths = []
		for oldPos in self.__lastAttackPosition:
			for newPos in self.__currAttackPosition:
				#print(oldPos + ' -> ' + newPos)
				#print(list(nx.all_simple_paths(self.G, source=oldPos, target=newPos)))
				self.__attackPaths = self.__attackPaths + list(nx.all_simple_paths(self.G, source=oldPos, target=newPos))
				uni_data=[]
				for dat in self.__attackPaths:
					if dat not in uni_data:
						uni_data.append(dat)
				self.__attackPaths = uni_data
		#print(self.__attackPaths)

	def __getAllEnds(self):
		self.__lastAttackPosition = []
		for dat in self.__attackPaths:
			if dat[-1] not in self.__lastAttackPosition:
				self.__lastAttackPosition.append(dat[-1])

	def __getAllPossibleNextPaths(self):
		self.__nextPaths = []
		print("GENERATE PATH FROM:")
		print(self.__lastAttackPosition)
		print("GENERATE PATH TO:")
		print(self.__monitoringCriticalNodes)
		for oldPos in self.__lastAttackPosition:
			for newPos in self.__monitoringCriticalNodes:
				#print(oldPos + ' -> ' + newPos)
				#print(list(nx.all_simple_paths(self.G, source=oldPos, target=newPos)))
				self.__nextPaths = self.__nextPaths + list(nx.all_simple_paths(self.G, source=oldPos, target=newPos))
				uni_data=[]
				for dat in self.__nextPaths:
					if dat not in uni_data:
						uni_data.append(dat)
				self.__nextPaths = uni_data
				self.pathPrismState(uni_data)

	def __getPathWithDefense(self):
		self.__nodesWithDeff = [x for x,y in self.G.nodes(data=True) if 'data' in y and len(y['data'].deffenses) > 0]
		self.__pathsWithDef = [x for x in self.__nextPaths if len([y for y in x if y in self.__nodesWithDeff]) > 0 ]

	def parsePrismStates(self):
		file = SOURCE_DIRECTORY + "sta.sta"
		with open(file, 'r') as myfile:
			data=myfile.readlines()
		self.__prismStates = {}
		for line in data:
			state = re.findall(r"^\d+:", line)
			if(len(state) == 0):
				continue
			state = state[0][:-1]
			params = re.search(r"\(([\s\S]+)\)", line)
			params = params.group(1)
			paramsArr = params.split(',')
			lineId = ''
			afterParams = True
			#print(paramsArr)
			#print(paramsArr[-1])
			#if "doDef" in str(self.__prismTransitions[state]) or "defC" in str(self.__prismTransitions[state]):
			#	print("X" + str(paramsArr[-1]) + "-" + str(self.__prismTransitions[state]))
			if paramsArr[-1] != "2":
				continue
			for prm in paramsArr:
				#print(prm)
				if (prm == 'true' or prm == 'false') and afterParams:
					lineId += prm[:1]
				# add defender resources
				else:
					lineId += prm
					
			#print(str(self.__prismTransitions[state]))
			#if "NOP" not in str(self.__prismTransitions[state]):
			#	print(self.__prismTransitions[state])
			print(lineId)

			self.__prismStates[lineId] = state
		#print(self.__prismStates)

	def parsePrismStrategy(self):
		file = SOURCE_DIRECTORY + "defStrategy.adv"
		with open(file, 'r') as myfile:
			data=myfile.readlines()
		self.__prismStrategy = {}
		for line in data:
			strategy = re.findall(r"^\d+ ", line)
			if(len(strategy) == 0):
				continue
			stateId = strategy[0][:-1]
			params = line.replace(r"^\d+ ", "")
			#print(">" + stateId + "<")
			paramsArr = params.split(' ')
			#for par in paramsArr:
			#	print('')
				
			self.__prismStrategy[stateId] = line
		#print(self.__prismStates)

	def parsePrismTransitions(self):
		file = SOURCE_DIRECTORY + "prismMap.tra"
		with open(file, 'r') as myfile:
			data=myfile.readlines()
		self.__prismTransitions = {}
		firstLine = True
		for line in data:
			if firstLine:
				firstLine = False
				continue

			stateId = re.findall(r"^\d+ ", line)
			if(len(stateId) == 0):
				continue
			stateId = stateId[0][:-1]
			#print(">" + stateId + "<")
			paramsArr = line.split(' ')
			if not stateId in self.__prismTransitions:
				self.__prismTransitions[stateId] = {}
			self.__prismTransitions[stateId][paramsArr[1]] = paramsArr[-1].strip()
			#if "doDef" in str(paramsArr[-1]) or "defC" in str(paramsArr[-1]):
			#	print(paramsArr[-1])
		#print(self.__prismStates)
		

	def pathPrismState(self, path):
		global PRISM_GLOBAL

		out = []
		idState = ""
		prismState = ""
		#print(path)
		
		for subLength in range(0, len(path)):
			subPath = path[0:subLength]
			#print(subPath)
			idState = ""
			for attNode in self.__attackerNodes:
				#print(attNode)
				if attNode in subPath:
					idState += "t"
				else:
					idState += "f"
			
			idState += "0" # separator
			idState += "f" # node0
			
			for prismGlobal in PRISM_GLOBAL:
				val = PRISM_GLOBAL[prismGlobal]
				idState += val[0]

			idState += "2" # defender step
			

			#print(idState)
			#out.append(idState)
			print(idState)
			if idState in self.__prismStates:
				idNumState = self.__prismStates[idState]
				prismState += self.__prismStrategy[idNumState] + " "
			else:
				print("NENI")

		return prismState

		#print(idState)
		if idState in self.__prismStates:
			prismState = self.__prismStates[idState]
			
			return prismState
		return ""
			

	def updatePrismGlobals(self, state):
		global PRISM_GLOBAL
		
		file = SOURCE_DIRECTORY + "input.prism"
		with open(file, 'r') as myfile:
			data=myfile.readlines()
		
		for line in data:
			if re.match("\[" + state + "\]", line):
				print("PRISMMMMM " + line)
				words = line.split(" ")
				last = words[-1]
				value = last[:-1]
				name = words[1]
				PRISM_GLOBAL[name] = value


	def __doDefense(self):
		global defResources
		print("DO DEFENSE")
		defDistances = {}
		wasThere = {}
		possibleDefs = {}
		for path in self.__attackPaths:
			prismAllState = self.pathPrismState(path)
			print(str(len(prismAllState)))
			if prismAllState in wasThere or len(prismAllState) == 0:
				continue
			wasThere[prismAllState] = True
			if not NICE_OUT:
				print("STATES:")
				#
			print("ALL STATES" + prismAllState)	
			idStates = [s for s in prismAllState.split() if s.isdigit() and int(s) > 0]

			idCurrentState = idStates[0]
			print("STRATEGY IDS - " + str(self.__prismStrategy[idCurrentState]))
			print("STRATEGY - " + str(self.__prismTransitions[idCurrentState]))
			choiceStrategy = self.__prismTransitions[idCurrentState]['0']

			self.updatePrismGlobals(choiceStrategy)
			
			if defResources > 0:
				defResources = defResources - 1
			return
			self.pathPrismState1(path)
			print("NOT JUMP " + prismState)
			print("PRISM_STATE-" + prismState + "<")
			print("STRATEGY-" + str(self.__prismStrategy[prismState]))
			print("STRATEGY-" + str(self.__prismTransitions[prismState]))
			wasThere[prismState] = True
			for defN in self.__nodesWithDeff:
				try:
					defIndex = path.index(defN)
					if defN in defDistances and defDistances[defN] > defIndex:
						defDistances[defN] = defIndex
					else:
						defDistances[defN] = defIndex
					#print(defIndex)
				except:
					pass

		return

		print(defDistances)
		for defName in self.__nodesWithDeff:
			if not(defName in defDistances):
				continue
			hostname = self.G.node[defName]['data'].params[0]
			print(hostname)
			if defDistances[defName] < 50:
				ips = config.getIpFromHostname(hostname)
				print("protect:")
				#print(ips)
				#for ip in ips:
					#key = ADattNetwork.KeyStruct(host = ip, port = 80, vulnerability = "", secret = "admin")
					#if ADreq.CmdLocalRequest(ip, 80).send("runDefense", key) != False:
						#break