#!/usr/bin/python3

import sys
import json
import socket

from ADsim import service as ADservice, globalConfig as ADglobal, monitor as ADmonitor


def main():
	startServices()

def startServices():
	configFile = ""
	if len(sys.argv) > 1:
		configFile = sys.argv[1]

	config = ADglobal.getClientConfig(configFile)
	
	ADmonitor.SERVER_ADDRESS = "10.0.12.13"
	ADmonitor.runMonitor()

	for service in config['services']:
		ADservice.ThreadedService('', service['port'], service['name'], service['vulnerabilities']).listen()

def getIP():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	try:
		s.connect(('10.255.255.255', 1))
		IP = s.getsockname()[0]
	except:
		IP = '127.0.0.1'
	finally:
		s.close()
	return IP

if __name__ == "__main__":
	main()