import networkx as nx
import matplotlib.pyplot as plt
import re
import sys
from random import random
from decimal import Decimal

if len(sys.argv) < 2:
	print('2 params <inMulvalDot> <outPrism>')
	sys.exit()

mulvalDotFile: str = sys.argv[1] #"/home/fanda/Documents/mulval/sandbox/AttackGraph.dot";
outFile: str = sys.argv[2]
mulvalDefenseDotFile = ""
if len(sys.argv) > 3:
	mulvalDefenseDotFile = sys.argv[3]
	print(mulvalDefenseDotFile)

#print(mulvalDotFile);

#graphname="basic.dot"
#g = load_graph(mulvalDotFile,"dot")

#print(g.vertex(8))

def getProbability():
	r = Decimal(random())
	return round(r, 2)

def read_dot(path):
    """Return a NetworkX graph from a dot file on path.

    Parameters
    ----------
    path : file or string
       File name or file handle to read.
    """
    try:
        import pygraphviz
    except ImportError:
        raise ImportError('read_dot() requires pygraphviz ',
                          'http://pygraphviz.github.io/')
    A=pygraphviz.AGraph(file=path)
    return A

def actionName(node, preCondId):
	name = node[1]['label']
	name = replaceSpecChars(name)
	out = 'n' + preCondId + '__' + name #[2:]
	#print(out)
	return out

def replaceSpecChars(inStr):
	inStr = re.sub(r'\d+:', '', inStr, 1)
	inStr = re.sub(r'\):\d+', '', inStr, 1)
	inStr = inStr.replace('(', '_')
	inStr = inStr.replace(')', '_')
	inStr = inStr.replace(':', '_')
	inStr = inStr.replace(',', '_')
	inStr = inStr.replace(' ', '_')
	inStr = inStr.replace('.', '_')
	return inStr

def generateSuccessorsOr(G, nodeId):
	succNodes = [node for node in nx.dfs_successors(G, nodeId)]
	succNodes = set(succNodes)
	#print(succNodes)
	out = ''
	for succNodeId in succNodes:
		succData = G.node[succNodeId]
		if succData['shape'] != 'diamond':
			continue
		succName = 'node' + succNodeId
		out += succName+'|'
	out = out[:-1]
	return '(' + out + ')'

def getPredecessorsRecursive(g, start_node):          
	stack = [start_node]
	out = []

	while stack:
		node = stack.pop()
		out.append(node)
		preds = g.predecessors(node)
		for pred in preds:
			if not pred in out:
				stack.append(pred)
	return out

def getSuccessorsRecursive(g, start_node):          
	stack = [start_node]
	out = []

	while stack:
		node = stack.pop()
		out.append(node)
		preds = g.successors(node)
		for pred in preds:
			if not pred in out:
				stack.append(pred)
	return out

def isInSuccessors(G, nodeId, testNodeId):
	#remove connection with tested node
	#because we need to know if we need this node in future
	tmpG = G.copy()
	preConditions = tmpG.predecessors(nodeId)
	toRemove = []
	for preCondId in preConditions:
		preDiamond = tmpG.predecessors(preCondId)
		if testNodeId in preDiamond:
			toRemove.append(preCondId)
	for rmvId in toRemove:
		tmpG.remove_node(rmvId)
	
	succNodes = [node for node in getSuccessorsRecursive(tmpG, nodeId)]
	
	for succNodeId in succNodes:
		preNodes = getPredecessorsRecursive(tmpG, succNodeId)
		if testNodeId in preNodes:
			return True
	
	return False

def generateUnusedNodes(G, nodeId):
	allNodes = [x for x,y in G.nodes(data=True) if y['shape']=='diamond']
	allNodes = set(allNodes)
	succNodes = [node for node in nx.dfs_preorder_nodes(G, nodeId)]
	succNodes = set(succNodes)
	out = ''
	for oneNodeId in allNodes:
		if not oneNodeId in succNodes:
			out += '(node' + oneNodeId + "'=false)&"
	out = out[:-1]
	return out

G = nx.DiGraph( read_dot(mulvalDotFile) );

GDef = nx.DiGraph()
if len(mulvalDefenseDotFile) > 0:
	GDef = nx.DiGraph( read_dot(mulvalDefenseDotFile) );

#print(G.nodes.data())

#filtr dle parametru
#filterBoxs = [x for x,y in G.nodes(data=True) if y['shape']=='box']

class MulvalNode:
	id = ""
	actionName = ""
	params = []
	attacks = []
	deffenses = []

	def __init__(self, label):
		self.parse(label)

	def parse(self, label):
		nodeIdMatchObj = re.match(r'\d+:', label)
		self.id = nodeIdMatchObj.group(0)[0:-1]
		
		cleanMsg = re.sub(r'\d+:', '', label, 1)
		cleanMsg = re.sub(r':\d+.*', '', cleanMsg)

		allParams = re.findall(r'\w+', cleanMsg)
		self.actionName = allParams[0]
		if len(allParams) > 1:
			self.params = allParams[1:]

	def __str__(self):
		return str(self.__class__) + ": " + str(self.__dict__)



prismDefenderActions = ''
prismAttackerActions = ''
prismAttackerActions2 = ''

prismDefenderModule = ''
prismAttackerModule = ''
prismAttackerModule2 = ''

prismDefenderSced = ''
prismAttackerSced = ''
prismAttackerSced2 = ''



#for node in filterBoxs:
#	G.remove_node(node)
prismGlobalVar = ''
prismPreconditions = {}

for node in GDef.nodes.data():
	nodeId = node[0]
	#current node name
	#nodeVarName = 'nodeDef' + nodeId
	#we must create global variable of current node
	#prismGlobalVar += 'global ' + nodeVarName + ' : bool init false;\n'
	preConds = GDef.predecessors(nodeId)
	nodeObj = MulvalNode(GDef.node[nodeId]['label'])
	

	for preCondId in preConds:
		preCondShape = GDef.node[preCondId]['shape']
		if preCondShape != "ellipse":
			continue
		#print(preCondId)
		#get all diamonds witch are connected to ellipse
		prismNodeLabel = GDef.node[preCondId]['label']
		
		defName = re.findall(r'\(([\w\W]+)\)', prismNodeLabel)[0]
		defName = replaceSpecChars(defName)
		if defName == "NO_DEFENSE":
			continue
		defName += "_" + nodeObj.params[0]
		preCondNodes = GDef.predecessors(preCondId)
		prismNodeAction = actionName(node, preCondId)
		prismNodePreconds = ''#'defRes>0&'
		prismNodePrecondsUpdate = '(' + defName + '\'=true)'
		isDefenderPreconds = False
		if not defName in prismPreconditions:
			prismPreconditions[defName] = 'false'
		
		#add each diamond to pre-condition
		for preCondNodeId in preCondNodes:
			preCondNodeName = 'nodeDef' + preCondNodeId
			preCondNodeLabel = GDef.node[preCondNodeId]['label']
			preCondNodeShape = GDef.node[preCondNodeId]['shape']
			if preCondNodeShape == "box":
				preCondNodeObj = MulvalNode(preCondNodeLabel)
				preCondNodePrism = preCondNodeObj.actionName + "_" + preCondNodeObj.params[0]
				if not preCondNodeObj.actionName.startswith("dynamic"):
					continue
				prismNodePreconds += preCondNodePrism + '&'
				prismPreconditions[preCondNodePrism] = "true"

		#node do not have precondition
		if isDefenderPreconds and len(prismNodePreconds) == 0:
			continue

		#remove last & character
		prismNodePreconds = prismNodePreconds[:-1]

		prismDefenderSced += '	[' + prismNodeAction + '] ' + 't=2 -> (t\'=1);\n'


		prismNodeAction = '	[' + prismNodeAction + ']'

		prismDefenderActions += prismNodeAction + ', '

		probSucc = getProbability()
		probFail = 1 - probSucc
		probSucc = str(probSucc)
		probFail = str(probFail)
		prismDefenderModule += prismNodeAction + ' ' + prismNodePreconds + ' -> ' + probSucc + ': ' + prismNodePrecondsUpdate +  ' + ' + probFail + ': true;\n' #
		# can not do defense
		# prismDefenderModule += prismNodeAction + ' !(' + prismNodePreconds + ') -> ' + 'true;\n'







#[connectH1] as=0 -> 0.85 : (as'=1) + 0.15 : (as'=as);
#[connectH1] t=1 -> (t'=2);
for node in G.nodes.data():
	#we want only diamond
	if node[1]['shape'] != 'diamond':
		continue
	
	nodeLabel = node[1]['label']
	#we want only attacker step
	if re.match(r'\d+:def[A-Z]{1}.*', nodeLabel, re.M):
		continue

	nodeId = node[0]
	nodeObj = MulvalNode(G.node[nodeId]['label'])
	#current node name
	nodeVarName = 'node' + nodeId
	#we must create global variable of current node
	prismGlobalVar += 'global ' + nodeVarName + ' : bool init false;\n'
	preConds = G.predecessors(nodeId)
	#print(node)
	
	#generate defense
	nodeHasDefenderActions = False
	postConds = G.successors(nodeId)
	for postCondId in postConds:
		postCondNodes = G.predecessors(postCondId)
		for postCondNodeId in postCondNodes:
			postCondNodeLabel = G.node[postCondNodeId]['label']
			#if defender has action
			if re.match(r'\d+:def[A-Z]{1}.*', postCondNodeLabel, re.M):
				prismNodeDefAction = actionName([0, G.node[postCondNodeId]], nodeId)
				prismDefenderActions += '[' + prismNodeDefAction + '], ' 
				prismDefenderModule += '	[' + prismNodeDefAction + '] ' + nodeVarName + ' -> 0.85:(' + nodeVarName + '\'=false) + 0.15: true;\n'
				# can not defense
				#prismDefenderModule += '	[' + prismNodeDefAction + '] !(' + nodeVarName + ') -> true;\n'
				prismDefenderSced += '	[' + prismNodeDefAction + '] ' + 't=2 -> (t\'=1);\n'
				nodeHasDefenderActions = True



	#for each pre ellipse (pre-condition)
	for preCondId in preConds:
		#print(preCondId)
		#get all diamonds witch are connected to ellipse
		preCondNodes = G.predecessors(preCondId)
		prismNodeAction = actionName(node, preCondId)
		prismNodePreconds = '' #'attRes>0&'
		prismNodePrecondsUpdate = '' #'&(attRes\'=attRes-1)'
		if nodeObj.actionName == "execCode":
			prismUpdateName = 'dynamicDefHasAccess_' + nodeObj.params[0]
			if prismUpdateName in prismPreconditions:
				prismNodePrecondsUpdate += '&(' + prismUpdateName + '\'=false)'

		isDefenderPreconds = False
		#add each diamond to pre-condition
		for preCondNodeId in preCondNodes:
			preCondNodeName = 'node' + preCondNodeId
			preCondNodeLabel = G.node[preCondNodeId]['label']
			preCondNodeShape = G.node[preCondNodeId]['shape']
			if preCondNodeShape == "diamond":
				if preCondNodeName == nodeVarName:
					continue
				if re.match(r'\d+:def[A-Z]{1}.*', preCondNodeLabel, re.M):
					isDefenderPreconds = True
					continue
				# updating states
				#prismNodePreconds +=  generateSuccessorsOr(G, preCondNodeId) + '&' #preCondNodeName + '&'
				# without updating - whole time true
				prismNodePreconds +=  preCondNodeName + '&'
				#if not isInSuccessors(G, nodeId, preCondNodeId):
				#	prismNodePrecondsUpdate += '&(' + preCondNodeName + '\'=false)'
			if preCondNodeShape == "box":
				if not preCondNodeLabel.startswith("dynamic"):
					continue
				prismNodePreconds += preCondNodeLabel + '&'
				prismPreconditions[preCondNodeLabel] = "true"

		#node do not have precondition
		if isDefenderPreconds and len(prismNodePreconds) == 0:
			continue

		#node is start node
		if len(prismNodePreconds) == 0:
			prismNodePreconds = 'node0 '
			prismNodePrecondsUpdate += '&(node0\'=false)'
		
		#remove last & character
		prismNodePreconds = prismNodePreconds[:-1]

		#prismNodeAction2 = '[' + prismNodeAction + '_2' + ']'
		prismNodeAction = '	[' + prismNodeAction + ']'

		prismAttackerActions += prismNodeAction + ', '
		#prismAttackerActions2 += prismNodeAction2 + ', '

		probSucc = getProbability()
		probFail = 1 - probSucc
		probSucc = str(probSucc)
		probFail = str(probFail)

		extendApply = False
		if nodeObj.actionName == "execCode":
			cond = 'DEF_RUN_ANTIVIRUS_' + nodeObj.params[0]
			if cond in prismPreconditions:
				extendApply = True
				prismAttackerModule += prismNodeAction + ' ' + prismNodePreconds +  '&!' + cond + ' -> '+probSucc+': (' + nodeVarName + '\' = true) ' + prismNodePrecondsUpdate +  ' + '+probFail+': true;\n' 
				prismAttackerModule += prismNodeAction + ' ' + prismNodePreconds +  '&' + cond +  ' -> 0.05: (' + nodeVarName + '\' = true) ' + prismNodePrecondsUpdate +  ' + 0.95: true;\n' 
		#scanAddress
		elif nodeObj.actionName == "scanAddress":
			cond = 'DEF_SWITCH_ADDRESS_' + nodeObj.params[0]
			if cond in prismPreconditions:
				extendApply = True
				prismAttackerModule += prismNodeAction + ' ' + prismNodePreconds +  '&!' + cond + ' -> '+probSucc+': (' + nodeVarName + '\' = true) ' + prismNodePrecondsUpdate +  ' + '+probFail+': true;\n' 
				prismAttackerModule += prismNodeAction + ' ' + prismNodePreconds +  '&' + cond +  ' -> 0.05: (' + nodeVarName + '\' = true) ' + prismNodePrecondsUpdate +  ' + 0.95: true;\n' 
		
		if not extendApply:
			prismAttackerModule += prismNodeAction + ' ' + prismNodePreconds + ' -> '+probSucc+': (' + nodeVarName + '\' = true) ' + prismNodePrecondsUpdate +  ' + '+probFail+': true;\n' #
		#prismAttackerModule2 += prismNodeAction2 + ' ' + prismNodePreconds + ' -> 0.85: (' + nodeVarName + '\' = true) ' + prismNodePrecondsUpdate +  ' + 0.15: true;\n' #

		#optimalization, we do not need NOP steep
		if nodeHasDefenderActions:	 
			prismAttackerSced += prismNodeAction + 't=1 -> (t\'=2);\n'
			#prismAttackerSced += prismNodeAction + 't=1 -> (t\'=1);\n'
		else:
			prismAttackerSced += prismNodeAction + 't=1 -> (t\'=2);\n'
			#prismAttackerSced += prismNodeAction + 't=1 -> (t\'=1);\n'

		#prismAttackerSced2 += prismNodeAction2 + 't=2 -> (t\'=3);\n'

prismDefenderActions = prismDefenderActions[:-2]
prismAttackerActions = prismAttackerActions[:-2]
#prismAttackerActions2 = prismAttackerActions2[:-2]
#print(prismAttackerActions)
#print(prismAttackerSced)

prismGlobalVar += "\n"

prismGlobalVar += 'global split : int init 0; // for split prism output in KB \n'
prismGlobalVar += 'global node0 : bool init true;\n'

for attr, value in prismPreconditions.items():
	prismGlobalVar += 'global ' + attr + ' : bool init ' + value + ";\n"


#generate prism file
prismSrc = ''
prismSrc += """
smg

player a1
  attacker1, """ + prismAttackerActions + """
endplayer 

player def
  defender, """ + prismDefenderActions + """ , [defNOP]
endplayer

""" + prismGlobalVar + """

module attacker1
""" + prismAttackerModule + """
endmodule

""" + """

module defender

""" + prismDefenderModule + """
	 [defNOP] true -> true; // if defender can not defend
	
endmodule

module sched
	t: [1..2] init 1;

""" + prismAttackerSced + """

""" + prismAttackerSced2 + """

	
""" + prismDefenderSced + """
	 [defNOP] t=2 -> (t'=1);

endmodule
"""

file = open(outFile, "w") 
file.write(prismSrc)
file.close() 


#G.predecessors('b')