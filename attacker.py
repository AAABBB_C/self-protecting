#!/usr/bin/python3


from ADsim import request as ADreq, attackerNetwork as ADnet, autonomeAttack as ADauto

def main():
	#port = 80
	#ADreq.AttackRequest('10.0.0.10', port).send()
	#ADreq.CmdLocalRequest('10.0.0.10', port).send("getKnownHosts")
	#ADreq.AttackRequest('10.0.0.11', port, ['10.0.0.10']).send()
	#ADreq.AttackRequest('10.0.1.11', port, ['10.0.0.11']).send()
	#ADreq.AttackRequest('10.0.2.11', port, ['10.0.0.11', '10.0.1.11']).send()
	#CmdRemoteRequest('10.0.3.10', port, ['10.0.2.20']).send(MsgStruct(attack='CVS-2016-0051'))
	#ADnet.Network.draw()
	
	autoAtt = ADauto._AutonomeAttack(['10.0.0.10'])
	autoAtt.start()

if __name__ == "__main__":
    main()